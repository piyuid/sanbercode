<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    public function create() {
        return view('pertanyaan.create');
    }

    public function store(Request $request) {
        $validatedData = $request->validate([
            'judul' => 'required|unique:posts',
            'isi' => 'required',
        ]);

        $query = DB::table('tanyaforum')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'pertanyaan gagal disimpan!');
    }

    public function index() {
        $posts = DB::table('tanyaforum')->get();
        return view('pertanyaan.index', compact('tanyaforum'));
    }

    public function show($id) {
        $post = DB::table('tanyaforum')->where('pertanyaan_id', $id)->first();

        return view('pertanyaan.show', compact('post'));
    }

    public function edit($id) {
        $post = DB::table('tanyaforum')->where('pertanyaan_id', $id)->first();

        return view('pertanyaan.edit', compact('post'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'judul' => 'required|unique:tanyaforum',
            'isi' => 'required'
        ]);

        $query = DB::table('tanyaforum')
                    ->where('pertanyaan_id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['isi']
                    ]);
        return redirect('../pertanyaan')->with('success', 'Berhasil diupdate gan!');
    }

    public function destroy($id) {
        $query = DB::table('tanyaforum')->where('pertanyaan_id', $id)->delete();
        return redirect('../pertanyaan')->with('success', 'Pertanyaan berhasil dihapus!');
    }
}
