<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTanyaforumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tanyaforum', function (Blueprint $table) {
            $table->id()->nullable(false)->change();
            $table->text('judul');
            $table->longText('isi');
            $table->date('tanggal_dibuat');
            $table->date('tanggal_diperbaharui');
            $table->integer('jawaban_ok_id');
            $table->integer('profil_id');

            $table->integer('pertanyaan_id')->nullable(false)->change();
            $table->integer('profil_id')->nullable(false)->change();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tanyaforum');
    }
}
