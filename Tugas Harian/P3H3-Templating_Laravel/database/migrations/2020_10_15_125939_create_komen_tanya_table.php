<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomenTanyaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komen_tanya', function (Blueprint $table) {
            $table->id();
            $table->uuidMorphs('isi');
            $table->date('tanggal_dibuat');
            $table->timestamps();

            $table->integer('pertanyaan_id')->nullable(false)->change();
            $table->integer('profil_id')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komen_tanya');
    }
}
