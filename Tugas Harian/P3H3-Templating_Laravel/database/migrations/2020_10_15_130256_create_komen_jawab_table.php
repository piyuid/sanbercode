<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomenJawabTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komen_jawab', function (Blueprint $table) {
            $table->id()->nullable(false)->change();
            $table->uuidMorphs('isi');
            $table->date('tanggal_dibuat');

            $table->integer('pertanyaan_id')->nullable(false)->change();
            $table->integer('profil_id')->nullable(false)->change();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komen_jawab');
    }
}
