@extends('master')

@section('content')
<div class="card card-warning">
    <div class="card-header">
    <h3 class="card-title">Edit Pertanyaan {{ $tanyaforum->pertanyaan_id }} !</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form class="form-horizontal" action="../pertanyaan/{{ $tanyaforum->pertanyaan_id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
          <div class="form-group">
            <label for="judul" class="col-sm-2 col-form-label">Judul</label>
              <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', $tanyaforum->judul) }}" placeholder="Isi Judul">
                @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
          </div>
          <div class="form-group">
            <label for="isi" class="col-sm-2 col-form-label">Deskripsi</label>
              <input type="text" class="form-control" id="isi" name="isi" value="{{ old('isi', $tanyaforum->isi) }}" placeholder="Isi Pertanyaan kamu">
              @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <button type="submit" class="btn btn-info">Buat!</button>
          <button type="submit" class="btn btn-default float-right">Cancel</button>
        </div>
        <!-- /.card-footer -->
      </form>
</div>
@endsection
