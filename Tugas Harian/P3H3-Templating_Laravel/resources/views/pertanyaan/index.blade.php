@extends('master')

@section('content')
    <div class="mt-3 mt-3">
        <div class="card">
            <div class="card-header">
                @if (session('success'))
                    {{ session('success') }}
                @endif
              <h3 class="card-title">Data Table Pertanyaan</h3>

              <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 350px; margin-right: 20px">
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default" href="../pertanyaan/create"><i class="fas fa-pen"></i>  Tulis Pertanyaan</button>
                    </div>
                </div>
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                  <div class="input-group-append">
                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
              <table class="table table-hover text-nowrap">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Isi Pertanyaan</th>
                    <th>Tanggal dibuat</th>
                    <th>Tanggal diperbaharui</th>
                    <th>Data</th>
                    <th>Perlihatkan</th>
                    <th>Editor</th>
                    <th>Drop</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($posts as $key => $tanyaforum)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $tanyaforum->judul }}</td>
                            <td>{{ $tanyaforum->isi }}</td>
                            <td>{{ $tanyaforum->tanggal_dibuat }}</td>
                            <td>{{ $tanyaforum->tanggal_diperbaharui }}</td>
                            <td> <a href="../pertanyaan/{{ $tanyaforum->pertanyaan_id }}"></a></td>
                        <td><a href="../pertanyaan/{{ $tanyaforum->pertanyaan_id }}" class="btn btn-info btn-sm">Show</a></td>
                            <td><a href="../pertanyaan/{{ $tanyaforum->pertanyaan_id }}/edit" class="btn btn-warning btn-sm">edit</a></td>
                        <td>
                        <form action="../pertanyaan/{{ $tanyaforum->id }}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                        </td>
                        </tr>
                    @empty($record)
                        <h3>Kosong!</h3>
                        <tr>
                            <td colspan="4" align="center">Tidak ada Pertanyaan</td>
                        </tr>
                    @endempty
                    @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
    </div>
@endsection
