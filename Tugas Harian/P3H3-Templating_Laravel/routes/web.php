<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('items.data1');
});

Route::get('/data-tables', function() {
    return view('items.data2');
});

// Route menuju Controller untuk Tugas CRUD

Route::get('/pertanyaan', 'App\Http\Controllers\PertanyaanController@index');
Route::get('/pertanyaan/create', 'App\Http\Controllers\PertanyaanController@create');

Route::post('/pertanyaan', 'App\Http\Controllers\PertanyaanController@store');
Route::get('/pertanyaan/{pertanyaan_id}', 'App\Http\Controllers\PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'App\Http\Controllers\PertanyaanController@edit');
Route::put('/pertanyaan/{pertanyaan_id}', 'App\Http\Controllers\PertanyaanController@update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'App\Http\Controllers\PertanyaanController@destroy');
